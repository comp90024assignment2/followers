"""
Team 21
Jack Stinson - 695666
Michael Milton
Koray Edib - 805529
Scott Lee - 793264
Jordan Lo Presti -640011

scrape_followers.py - python application that gathers followers from the 18
AFL team's official pages for tweet harvesting use later.
"""

import tweepy
import time
from cloudant import couchdb
from cloudant.document import Document
from requests.exceptions import Timeout, ConnectionError
from requests.packages.urllib3.exceptions import ReadTimeoutError
import ssl
import argparse

auth = tweepy.OAuthHandler(
    'yUc6mwn0d0Qu3Tt3Lb5HQDPPh',
    'DYXtlw3v5hmhTgFADsOhF5IbLMYgPrPlOQsPS3a76xiMYpi0VZ'
)
auth.set_access_token(
    '991251361810403328-0H07DWEUrpjDHdoh5W52eyl2h5cyUSs',
    'Rlj4XQY4TfBNlm74wDV9dpHQocmIXGwz6a1wSWN3tx24v'
)
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

team_handles = {
    'Adelaide': 'Adelaide_FC',
    'Carlton': 'CarltonFC',
    'Collingwood': 'CollingwoodFC',
    'Essendon': 'EssendonFC',
    'Fremantle': 'freodockers',
    'Geelong': 'GeelongCats',
    'GWS': 'GWSGIANTS',
    'Hawthorn': 'HawthornFC',
    'Melbourne': 'melbournefc',
    'North_Melbourne': 'NMFCOfficial',
    'Port_Adelaide': 'PAFC',
    'Richmond': 'Richmond_FC',
    'St_kilda': 'stkildafc',
    'West_Coast': 'WestCoastEagles',
    'Western_Bulldogs': 'westernbulldogs',
    'Brisbane': 'brisbanelions',
    'Gold_Coast': 'GoldCoastSUNS',
    'Sydney': 'sydneyswans'
}

current_teams = {}
current_users = {}


def get_args():
    parser = argparse.ArgumentParser('Scrapes twitter followers')
    parser.add_argument(
        '--num',
        '-n',
        type=int,
        help='Number of pages of followers to scrape for each team before moving onto the next team',
        default=1
    )
    parser.add_argument(
        '--db-host',
        help='The URL at which to connect to the database',
        required=True
    )
    return parser.parse_args()


def update_team_list():
    """
    Updates the dictionary of teams that are already in the DB
    """
    print('Updating team list...', end='')
    for team in db.get_view_result('_design/teams', view_name='all', include_docs=True, reduce=False):
        doc = team['doc']
        current_teams[doc['name']] = doc
    print('done')


def update_user_list(team):
    """
    Updates the dictionary of users that are already in the DB
    """
    print('Updating user list...', end='')
    for user in db.get_view_result('_design/users', view_name='byTeamToUser', key=team, include_docs=False,
                                   reduce=False):
        current_users[user['value']] = user['id']
    print('done')


args = get_args()
with couchdb('admin', 'Vai9vah6', url=args.db_host) as couch:
    db = couch['comp90024']
    print('Connected to CouchDB')

    print('Adding teams that are missing...')
    update_team_list()
    for team, handle in team_handles.items():
        if team not in current_teams:
            print(f'Adding {team}')
            doc = db.create_document({
                'name': team,
                'followers': [],
                'type': 'team',
                'cursor': -1,
                'handle': handle
            })
            current_teams[team] = doc

    while True:
        print('Starting the global loop')

        # Grab the team with the least users
        team = db.get_view_result(
            '_design/teams',
            include_docs=True,
            view_name='byLowestCursor',
            descending=False,
            reduce=False,
            limit=1
        )[0][0]['doc']
        cursor_number = team['cursor'] if 'cursor' in team else -1
        print(f'Scraping {team["name"]}, whose cursor is at {cursor_number}')

        update_user_list(team['name'])

        try:
            cursor = tweepy.Cursor(api.followers_ids, screen_name=team_handles[team['name']], cursor=cursor_number).pages(limit=args.num)
            for page in cursor:

                # Iterate the items in this page
                for user_id in page:
                    print(f'Scraping {user_id}')

                    if user_id not in current_users:
                        # If this is a new user, add it to the DB
                        print(f'{user_id} is new. Adding...')
                        doc = db.create_document({
                            'user_id': user_id,
                            'teams': [team['name']],
                            'last_scraped': 0,
                            'type': 'user'
                        })
                        current_users[user_id] = doc['_id']
                    else:
                        # If this is an existing user, just update their team list
                        doc = db[current_users[user_id]]
                        if team['name'] not in doc['teams']:
                            print(f'{user_id} is already in the database. Updating...')
                            doc.update_field(Document.list_field_append, 'teams', team['name'])
                        else:
                            print(f'{user_id} is already in the database for this team. Skipping...')

                # Update the team's cursor
                team_doc = db[team['_id']]
                team_doc.update_field(Document.field_set, 'cursor', cursor.next_cursor)

        except (Timeout, ssl.SSLError, ReadTimeoutError, ConnectionError, tweepy.TweepError) as e:
            print(f'Received an error while scraping: {e}')
            print('Sleeping for 60 seconds')
            time.sleep(60)

        print(f'Restarting global loop')
